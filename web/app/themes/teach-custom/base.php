<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <style>@supports (display: grid) {#grid-support{display:none;}}</style>
  <body <?php body_class(); ?>>
    <section id="grid-support">
        <p class="text-center">You are using an <strong>outdated</strong> browser. This site was developed with modern browsers in mind. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your viewing experience.</p>
    </section>
    <?php get_template_part('partials/skip-to');?>
    <?php do_action('get_header'); ?>
    <div class="grid" id="master-grid">
      <?php get_template_part('partials/navigation');?>
      <?php  if(is_front_page()) { get_template_part('templates/home-header'); }?>
  
      <main class="main grid" role="document" id="content">
        <?php include Wrapper\template_path(); ?>
      </main>
      
      <?php
        get_template_part('partials/partners');
        get_template_part('partials/contact-bar');
        do_action('get_footer');
        get_template_part('templates/footer');?>
    </div>
  <?php wp_footer(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-99028109-7"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-99028109-7');
    </script>

  </body>
</html>
