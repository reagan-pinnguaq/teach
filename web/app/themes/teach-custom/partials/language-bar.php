<section id="language-selector">

	<?php global $lang;
	$current = $lang->get_language(true);

	foreach($lang->get_languages() as $full_language => $language):?>
		<a href="?lang=<?=$language;?>" alt="<?=$full_language;?>"><?=$language;?></a>
	<?php endforeach;?>

</section>