<form id="contact-form" class="grid" method="POST" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
	<input type="hidden" name="action" value="contact_action_hook"/>
	<input type="text" name="name" placeholder="Name" required/>
	<input type="email" name="email" placeholder="Email" required/>
	<input type="text" name="subject" placeholder="Subject" required/>
	<textarea name="message" rows="10" placeholder="Message" required/></textarea>
	<input type="submit" value="Send"/>
</form>