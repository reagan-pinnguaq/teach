<?php global $lang;?>

<section id="partners" class="grid" >

	<h2>Our Partners</h2>
	<?php wp_nav_menu(['theme_location'=>'partner_navigation', 'menu_id'=>'partners', 'menu_class'=> 'grid', 'container'=>false, 'walker' => new Custom_Walker_Nav_Menu()]);?>
</section>