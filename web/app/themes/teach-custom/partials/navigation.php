<?php global $lang;?>

<nav id="site-nav" class="grid">
	<?php get_template_part('partials/current-lang-selector');?>

	<section id="mobile-nav">
		<span></span>
		<span></span>
		<span></span>
	</section>

	<?php $lang->menu('primary', 'primary-nav', false, 'mobile-collapsed', new Simplified_Walker_Nav_Menu());?>
	<a href="/" class="link-wrapper nav-logo"><h1><img src="<?=get_site_icon_url(); ?>"><span class="sr-only"><?=bloginfo('name');?></span></h1></a>
	<div class="background-bar"></div>
</nav>
