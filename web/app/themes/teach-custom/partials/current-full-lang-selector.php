<section id="full-language-selector" >

	<?php global $lang;	$current = $lang->get_language(true);?>

	<p class="current-language">
		<span class="sr-only">The current language is </span>
		<?=$current;?>
		
	</p>
	<span class="chevron bottom">

</section>

<ul id="full-language-popdown" class="collapsed" role="language-selection">
	<?php foreach($lang->get_languages() as $full_language => $language):?>
	<?php if($current != $full_language):?>
		<li><a href="?lang=<?=$language;?>" alt="<?=$full_language;?>" ><?=$full_language;?></a></li>
	<?php endif;?>
	<?php endforeach;?>
</ul>