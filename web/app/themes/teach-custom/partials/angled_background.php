<?php $background = get_field( 'featured_image' );?>
<div class="angled-background"></div>
<?php if($background && !is_archive()) : ?>
<div class="angled-background sub-background" style="background-image:url('<?=$background;?>');"></div>
<?php elseif(has_post_thumbnail()) :?>
<div class="angled-background sub-background" style="background-image:url('<?php get_the_post_thumbnail();?>');"></div>
<?php endif;?>
			
