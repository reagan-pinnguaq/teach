<?php $content_groups = [
	"introduction", "preparation", "sample", "instruction", "conclusion"
];?> 

<nav id="lesson-nav">
	
	<p class="mobile-only">Navigate To<span class="chevron bottom"></span></p>

	<?php foreach($content_groups as $content):?>
		<a href="#<?=$content?>" class="lesson-nav-item mobile-collapsed"><?=$content?></a>
		<?php endforeach;?>
</nav>