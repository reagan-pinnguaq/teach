
<form id="category-filter" method="POST" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
	<input type="hidden" name="action" value="filter_action_hook"/>
	<label id="category-toggle" for="category-filter" class="text-upper">Filter Lessons By	<span class="chevron bottom mobile-only"></span></label>
	<select name="category_filter" id="category-filter" class="text-upper mobile-collapsed" required>
		<option class="text-upper" value="" selected disabled>Subject<span class="chevron bottom"></span></option>
	<?php $terms = get_terms(['taxonomy' => 'category', 'hide_empty' => false,]);	?>
	<?php foreach($terms as $cat):?>
		<option class="text-upper" value="<?=$cat->term_id;?>"><?=$cat->name;?></option>
	<?php endforeach;?>
	</select>	
	<button type="submit" class="text-upper mobile-collapsed">Filter</button>
</form>