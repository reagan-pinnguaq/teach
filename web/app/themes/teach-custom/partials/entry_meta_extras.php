<?php $age_low =get_field( 'age_low_range' ); $age_high = get_field( 'age_high_range' );?>
<?php if($age_low && $age_high):?>
	<div class="recomended-age">
		<h3>Recomended Age:</h3>
		<p><?=$age_low;?> - <?=$age_high;?></p>
	</div> 
<?php endif;?>

<?php $lesson_duration =  get_field( 'lesson_duration'); $lesson_timescale = get_field( 'duration_selector' );?>
<?php if($lesson_duration && $lesson_timescale):?>
	<div class="recomended-time">
		<h3>Time To Complete:</h3>
		<p><?=$lesson_duration?> <?=$lesson_timescale;?></p>
	</div>
<?php endif;?>