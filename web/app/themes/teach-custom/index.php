<?php get_template_part('templates/page', 'header'); ?>

<?php  if(get_post_type() == 'curriculum'){get_template_part('partials/category_filter');}?>

<section class="archive-wrapper">
  <?php get_template_part('partials', 'empty_archive');?>
  <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
  <?php endwhile; ?>
</section>

<?php the_posts_navigation(); ?>
