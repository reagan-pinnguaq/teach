<?php 
namespace Roots\Sage\LanguageManager;
/* Language manager for custom theme. */ 

	// This is a class to manage alternate language configs the site.
	// Supports ACF. 
	class LanguageManager
	{
		private $_currentLanguage;
		private $_acf = false;
		private $_defaultLang = 'EN';
		private $_languages = array(
			'english' =>'EN',
			// 'inukitut' => 'IN',
			'french'=> 'FR'
		);
		
		
		public function __construct($config = false)
		{	
			$this->_supports_acf();
			$this->_manageLangSession();
		}

		public function is_default(){
			return ($this->_currentLanguage == $this->_defaultLang ? true : false);
		}

		public function get_language($full = false)
		{	
			if($full)
			{
				foreach($this->_languages as $key => $language){
					if($language == $this->_currentLanguage){
						return $key;
					}
				}
			}
			return $this->_currentLanguage;
		}

		public function get_languages(){
			return $this->_languages;
		}



		public function e_strings($arr = array()){
			if(!empty($arr)){
				$key = array_search($this->get_language(), $this->_languages);
				return $arr[$key];
			}
		}

		/*  
		* @Function: field
		* @Description: A function that handles acf requests
		* @param string $fieldName: the name of the acf field you wish to request
		* @param bool $get: conditional for echo or not
		* @returns void || string
		* 
		* Requires ACF to be installed & enabled. 
		* Requires the fields to be in the following foramt:
		* {lang}_{fieldname} 
		* eg: en_title_bar, oj_title_bar, fr_title_bar ... etc
		*/
		public function field($fieldName, $get = false, $debug = false)
		{		
				$ref = $this->prefix($fieldName);
				($debug ? dd([$fieldName, $ref, get_field($ref)]) : '???');
				if(get_field($ref)){
					return (!$get ? the_field($ref) : get_field($ref));
				}
				return false;	
		}

		/*  
		* @Function: group_field
		* @Description: A function that handles acf requests for groups fields
		* @param string $group:
		* @param string $field: conditional for echo or not
		* @param string|bool $attr:
		* @param bool $get: conditional for echo or not
		* @returns string|void
		* 
		* Requires ACF to be installed & enabled. 
		* Requires the fields to be in the following foramt:
		* 
				Group to be: {lang}_{fieldname}
					Field to be: {lang}_{fieldname} 
		* 
		*/
		public function group_field($groupName, $fieldName, $attr = false, $get = false)
		{
			$result = $this->e_group_field($groupName, $fieldName, $attr);
			if(!$get){ echo $result;} else { return $result;}
		}

		/*  
		* @Function: e_group_field
		* @Description: A function that handles acf requests for groups fields
		*
		* @param string 			$groupName		:
		* @param string 			$fieldName		: conditional for echo or not
		* @param string|bool 	$attr					:
		* @param string|bool 	$groupPrefix	:
		* @param string|bool 	$fieldPrefix	:
		* @returns bool|string
		* 
		* Requires ACF to be installed & enabled. 
		* Requires the fields to be in the following foramt:
		* 
				Group to be: {lang}_{fieldname}
					Nested Field to be: {lang}_{fieldname} 
		* 
		*/

		
		public function e_group_field($groupName, $fieldName, $attr = false, $groupPrefix = true, $fieldPrefix = true){
			// set prfixes
			if($groupPrefix) {$groupName = $this->prefix($groupName);}
			if($fieldPrefix) {$fieldName = $this->prefix($fieldName);}
			
			$groupArr = get_field($groupName);
			if($groupArr){
				return (!$attr ? $groupArr[$fieldName] : $groupArr[$fieldName][$attr]);
			}			
		}
		/*  
		* @Function: menu
		* @Description: a function that gives the appropiate menu based on language
		* @param string $menuName: the name of the 
		* @param string $menuId: the id that will wrap the menu
		* @param string||bool $container: name of class wrapper
		* @param string||bool $menuClass: classes for the menu.
		* @param class $walker: is an instance of a walker_nav_class
		* @returns void ? string
		* 
		* 
		* Requires two locations that are prefixed and have a menu attached
		* EG: {lang}_{location} 
		* eg: en_footer_nav, oj_footer_nav, fr_footer_nav ... etc
		*/

		public function menu($menuLocation, $menuId, $container, $menuClass = false, $walker = false)
		{
			if($container != false){
				$container = $this->prefix($container);
			}
			
			$config = array
			(
				'theme_location'	=> $this->prefix($menuLocation, '-'),
				'menu_id'					=> $menuId,
				'container' 			=> $container
			);
			if($menuClass){
				$config['menu_class'] = $menuClass;
			}
			if($walker)
			{
				$config['walker'] = $walker;
			}
			
			return wp_nav_menu($config);
		}

		/*  
		* @Function: option
		* @Description: a function that fetches a valid setting/option from the customize pannel
		* @param string $optionName: the id of the setting.
		* @returns mixed.
		*/

		public function option($optionName)
		{		
				$ref = $this->prefix($optionName, false, true);
				return get_theme_mod($ref);
		}

		/*
		* Function: prefix
		* @description: a funciton that prefixes the field for the correct language.
		* @param string $Fieldname: the name of the field to be prefixed
		* @param string $prefix: prefix seperator to use. Default is '_'
		* @param bool $key: to use the full word of the language type from lang list. Default is false.
		*/
		public function prefix($fieldName, $prefix = '_', $key = false, $default = false){
		 ($key ? $langPrefix = $langPrefix = array_search($this->_currentLanguage, $this->_languages) : $langPrefix = strtolower($this->_currentLanguage));
		 ($default ? $langPrefix = strtolower($this->_defaultLang) : '???');
				return $langPrefix . $prefix . $fieldName;
		}

		/* Class managers */
		private function _manageLangSession()
		{	
			$lang = null;
			(!isset($_SESSION) ? session_start(): '???'); 
			(isset($_SESSION['lang']) ? $lang =  $_SESSION['lang'] : '???');
			(isset($_GET['lang']) ? $lang =  $_GET['lang'] : '???');
			($lang == null ? $this->_setDefaultLang() : $this->_setValidLang($lang) );
			$this->_setLangSession();
		}

		private function _setDefaultLang()
		{
			$this->_currentLanguage = $this->_defaultLang;
		}

		private function _setLangSession()
		{
			$_SESSION['lang'] = $this->_currentLanguage;
		}

		private function _setValidLang($lang){
			(in_array($lang, $this->_languages) ? $this->_currentLanguage = $lang : $this->_setDefaultLang());
		}

		//Supports functions
		private function _supports_acf()
		{
			(
				class_exists('acf')
				? $this->acf = true //aka continue
				: trigger_error("Advanced Custom Fields Not Activated. Language Support Not Avaliable For ACF.", E_USER_NOTICE)
			);
		}
	}

	$lang = new LanguageManager();