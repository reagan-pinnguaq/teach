<?php

/*
    <nav class="social row no-wrap justify-content-end align-self-center">
        <a target="_blank" href="https://twitter.com/pinnguaq" class="twitter"></a>
        <a target="_blank" href="https://www.facebook.com/Pinnguaq/" class="facebook"></a>
        <a target="_blank" href="https://www.instagram.com/pinnguaq/" class="instagram"></a>
    </nav>
*/

class Simplified_Walker_Nav_Menu extends \Walker_Nav_Menu {
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= '<ul class="sub-menu">';
    }
  
    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= '</ul>';
    }
  
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $classes = array();
        if( !empty( $item->classes ) ) {
            $classes = (array) $item->classes;
        }
  
        $active_class = '';
        if( in_array('current-menu-item', $classes) ) {
            $active_class = ' class="active"';
        } else if( in_array('current-menu-parent', $classes) ) {
            $active_class = ' class="active-parent"';
        } else if( in_array('current-menu-ancestor', $classes) ) {
            $active_class = ' class="active-ancestor"';
        }
  
        $url = '';
        if( !empty( $item->url ) ) {
            $url = $item->url;
				}
				
				// pr($item);
  
                // $output .= '<li'. $active_class . '><a href="' . $url . '"><img src="'. get_field('icon', $item) .'" alt="'. $item->title .'"><span class="sr-only">' . $item->title . '</span></a></li>';
                if(isset($item->classes[0])){ $itemClasses = $item->classes[0];} else {$itemClasses = '';}
				$output .= '<li'. $active_class . '>
				<a target="'. $item->target .'" href="' . $url . '" class="' . $itemClasses . '" >
				 ' . $item->title . '
				 </a>
				 </li>';
    }
  
    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= '</li>';
    }
  }