<?php

namespace Roots\Sage\Titles;

/**
 * Page titles
 */
function title() {

  $category_select_term = get_field( 'category_select' );

  if (is_home()) {
    if (get_option('page_for_posts', true)) {
      return get_the_title(get_option('page_for_posts', true));
    } else {
      return __('Latest Posts', 'sage');
    }
  } elseif(is_category() && $category_select_term ){
    if ( $category_select_term ){
      return $category_select_term->name;
    }
  } elseif(get_post_type() == 'curriculum' && is_single()){
    $title = $lang->field('title');
    if($title){
      return $title;
    }
    return get_the_title();
  }
    elseif (is_archive()) {
    return get_the_archive_title();
  } elseif (is_search()) {
    return sprintf(__('Search Results for %s', 'sage'), get_search_query());
  } elseif (is_404()) {
    return __('Not Found', 'sage');
  } else {
    return get_the_title();
  }
}