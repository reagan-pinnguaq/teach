<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'en-primary' => __('English Primary Navigation', 'sage'),
    'fr-primary' => __('French Primary Navigation', 'sage'),
    'in-primary' => __('Inuktitut Primary Navigation', 'sage'),

    'en-footer' => __('English Footer Navigation', 'sage'),
    'fr-footer' => __('French Footer Navigation', 'sage'),
    'in-footer' => __('Inuktitut Footer Navigation', 'sage'),

    'en-information' => __('English Information Navigation', 'sage'),
    'fr-information' => __('French Information Navigation', 'sage'),
    'in-information' => __('Inuktitut Information Navigation', 'sage'),

    'partner_navigation' => __('Partner Navigation', 'sage'),
    'social_navigation' => __('Social Navigation', 'sage'),    
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

// /**
//  * Register sidebars
//  */
function widgets_init() {
  register_sidebar([
    'name'          => __('lesson', 'sage'),
    'id'            => 'sidebar-lesson',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);


/* Custom configs ahead */


//disable default Posts
add_action('admin_menu', __NAMESPACE__ .'\remove_default_post_type');

function remove_default_post_type() {
	remove_menu_page('edit.php');
}

//add post type to default category taxonomy
function query_post_type($query) {
  $post_types = get_post_types();
  if ( is_category() || is_tag()) {
      $post_type = get_query_var('curriculum');
      if ( $post_type ) {
          $post_type = $post_type;
      } else {
          $post_type = $post_types;
      }
      $query->set('post_type', $post_type);
      return $query;
  }
}

add_filter('pre_get_posts',  __NAMESPACE__ .'\query_post_type');



/* Removing "archive" in title hook */
function my_theme_archive_title( $title ) {
  if ( is_category() ) {
      $title = single_cat_title( '', false );
  } elseif ( is_tag() ) {
      $title = single_tag_title( '', false );
  } elseif ( is_author() ) {
      $title = '<span class="vcard">' . get_the_author() . '</span>';
  } elseif ( is_post_type_archive() ) {
      $title = post_type_archive_title( '', false );
  } elseif ( is_tax() ) {
      $title = single_term_title( '', false );
  }
  return $title;
}

add_filter( 'get_the_archive_title',  __NAMESPACE__ .'\my_theme_archive_title');

function acf_excerpt($text='')
{
	$text = strip_shortcodes( $text );
	$text = apply_filters('the_content', $text);
	$text = str_replace(']]>', ']]&gt;', $text);
	$excerpt_length = apply_filters('excerpt_length', 55);
	$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
	return wp_trim_words( $text, $excerpt_length, $excerpt_more );
}
add_filter('wp_trim_excerpt', __NAMESPACE__ .'\acf_excerpt');


function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__ .'\cc_mime_types');
function fix_svg_thumb_display() {
  echo '
  <style>
    td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { 
      width: 100% !important; 
      height: auto !important; 
    }
    </style>
  ';
}
add_action('admin_head', __NAMESPACE__ .'\fix_svg_thumb_display');

