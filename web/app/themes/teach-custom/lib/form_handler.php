<?php

/* Category filter redirector */
function filter_category()
{	
	$post = $_POST;
	$url = get_category_link($post['category_filter']);
	if(wp_redirect($url)){
		exit;
	} else {
		$url = get_site_url("curriculum");
		if(wp_redirect($url)){
			exit;
		}
	}
}

add_action( 'admin_post_nopriv_filter_action_hook', 'filter_category' );
add_action( 'admin_post_filter_action_hook', 'filter_category' );

function handle_message()
{	
	$post 		= $_POST;
	$subject 	= $post['subject'];
	$email 		= $post['email'];
	$name 		= $post['name'];
	$message	= $post['message'];

	$subject 	= 'Te(a)ch - '. $subject;
	
	$headers[] = 'Content-Type: text/html; charset=UTF-8';
	$headers[] = 'From: '. $name . ' <' . $email . '>';
	$recipient = 'reagan@pinnguaq.com';

	$mail = wp_mail($recipient, $subject, $message, $headers);

	if($mail)
	{
		$body = "Greetings " . $name .", we have recieved your message and just wanted to let you know that someone will be replying soon. /r/n";
		$body .= "On ". date('M-d') . ' at ' . date("H:i") . " you wrote: /r/n/r/n";
		$body .= $message;
		$uheaders[] = 'Content-Type: text/html; charset=UTF-8';
		$uheaders[] = 'From: Noreply <no-reply@teachnunavut.com>';

		wp_mail($email, $subject, $body, $uheaders);
	}

	if(wp_redirect(site_url() . '/contact')){
		exit;
	}
}

add_action( 'admin_post_nopriv_contact_action_hook', 'handle_message' );
add_action( 'admin_post_contact_action_hook', 'handle_message' );