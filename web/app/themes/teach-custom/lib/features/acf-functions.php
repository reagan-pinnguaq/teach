<?php


function acf_excerpt($text='', $length)
{

	
	$text = strip_shortcodes( $text );
	$text = apply_filters('the_content', $text);
	$text = str_replace(']]>', ']]&gt;', $text);
	
	$excerpt_length = apply_filters('excerpt_length', $length);
	$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
	
	$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
	return $text;
}
add_filter('wp_trim_excerpt','acf_excerpt');

?>