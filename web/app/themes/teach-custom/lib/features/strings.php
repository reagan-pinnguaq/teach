<?php
/*
 * String functions
 */
function limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
					$text = substr($text, 0, $pos[$limit]) . '... <a href="'. get_the_permalink() .'">Read More</a>' ;
					$text = closetags($text);
      }
      return $text;
		}
		
function closetags($html) {
		preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
		$openedtags = $result[1];
		preg_match_all('#</([a-z]+)>#iU', $html, $result);
		$closedtags = $result[1];
		$len_opened = count($openedtags);
		if (count($closedtags) == $len_opened) {
				return $html;
		}
		$openedtags = array_reverse($openedtags);
		for ($i=0; $i < $len_opened; $i++) {
				if (!in_array($openedtags[$i], $closedtags)) {
						$html .= '</'.$openedtags[$i].'>';
				} else {
						unset($closedtags[array_search($openedtags[$i], $closedtags)]);
				}
		}
		return $html;
	} 


	function get_content_between_tags($x){
		preg_match("/>(.*?)</", $x, $y);
		return $y[1];
	}