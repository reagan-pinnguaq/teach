<?php
/**
 * WP Object Fetches
 */
function get_page_by_name($page){
	$args = array('post_type' => 'page', 'pagename' => $page); 
	$result = new WP_Query( $args );
	return $result;
}

function get_archive_by_type($post_type){
	$args = array('post_type' => $post_type);
	$result = new WP_Query( $args );
	return $result;
}

function get_archive_by_type_orderby_custom_field($post_type, $order, $direction, $is_numb = false, $limit = false){
	if($is_numb == true){
		$meta = "meta_value_num";
	} else {
		$meta = "meta_value";
	}
	if($limit == false){
		$limit = -1;
	}
		$args = array(
		'post_type' 		=> $post_type,
		'posts_per_page'	=> $limit,
		'meta_key'			=> $order,
		'orderby'			=> $meta,
		'order'				=> $direction
	);
	$result = new WP_Query( $args );
	return $result;
}

function get_archive_by_type_where_value($post_type, $key, $value, $limit = false){
	if($limit == false){
		$limit = -1;
	}
	$args = array(
		'post_type'		=> $post_type,
		'numberposts'	=> $limit,
		'meta_key'		=> $key,
		'meta_value'	=> $value
	);
	$result = new WP_Query( $args );
	return $result;
}

