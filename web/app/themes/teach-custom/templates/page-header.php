<?php use Roots\Sage\Titles; global $lang; ?>

<header class="grid">
    <h2 class="text-center"><?= Titles\title(); ?></h2>
</header>
<?php get_template_part('partials/angled_background'); ?>