
<?php //get_template_part('partials/entry_meta_extras');?>


<?php $project_files = get_field( 'project_files' ); ?>
<?php if ( $project_files ): ?>
	<a class="btn btn-link" target="_blank" href="<?php echo $project_files['url']; ?>">Download Assets</a>
<?php endif; ?>
<?php $pdf_file = get_field( 'pdf_download' );?>
<?php if ( $pdf_file ): ?>
	<a class="btn btn-link" target="_blank" href="<?=$pdf_file;?>">Download PDF</a>
<?php endif; ?>