<?php global $lang;?>
<?php $svg = get_field( 'svg' ); //dd($svg); ?>
<header class="grid">
	<a href="/" class="link-wrapper" role="Link back to the home page"><h1 class="grid">
	<?php if ( $svg ) : ?>
		<img src="<?php echo $svg['url']; ?>" alt="<?=bloginfo('name');?> logo">
	<?php else : ?>	
	<img src="<?=get_site_icon_url(); ?>" alt="<?=bloginfo('name');?> logo">
	<?php endif;?>
	<span class="sr-only"><?=bloginfo('name');?></span></h1></a>
	
	<h2><?php $lang->field('intro_title');?></h2>
	<div class="false-line"></div>
	<?php $lang->field('intro_text');?>
	<div class="angled-background"></div>
</header>