<?php global $lang;
 while (have_posts()) : the_post(); ?>
  <article <?php post_class('grid'); ?>>
    <header class="grid">
      <h1 class="entry-title text-center"><?php $title = $lang->field('title', true); if($title){echo $title;} else {the_title();} ?></h1>
      <?php if(get_post_type() == 'curriculum'):?>
      <h2 class="entry-category text-center"><?php the_category();?></h2>
      <?php endif;?>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <?php get_template_part('partials/angled_background'); ?>
    <?php if(get_post_type() == 'curriculum'):?>
      <?php get_template_part('partials/lesson_navigation');?>
      <?php get_template_part('templates/content-lesson');?>
      <?php else:?>
      <div class="entry-content">
      <?php the_content(); ?>
      </div>
    <?php endif;?>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
