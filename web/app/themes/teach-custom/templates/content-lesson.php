<?php global $lang;
$content_groups = [
	"introduction", "preparation", "sample", "instruction", "conclusion"
];

foreach($content_groups as $content):?>

	<?php $content_field = $lang->prefix('content', '_' . $content . '_');?>

	<section id="<?=$content?>" class="grid">
		<h2 class="text-upper content-type"><?=$content;?></h2>
		<span class="chevron <?php if( isset($content) && $content != "introduction"):?>bottom<?php endif;?>"></span>	
		
		<?php /* do not change to get_template_part. Needs inherited $content_field! */
		include(locate_template('templates/blocks/content_block.php'));?>
		
		<?php if($content=="sample"):?>
			<a class="" href="<?=get_field('game_link');?>"><?=get_field('game_title');?></a>
		<?php endif;?>

		<?php if($content == 'introduction'):?>
			<div class="key-concept grid">
				<h4>Key Concept</h4>
				<p><?php $lang->field( 'key_concept' );?></p>
			</div>
		<?php elseif($content == 'conclusion'):?>
			<div class="final-thoughts grid">
				<h4>Final Thoughts</h4>
				<p><?php $lang->field( 'final_thoughts' );?></p>
			</div>
		<?php endif;?>
	</section>
<?php endforeach;?>