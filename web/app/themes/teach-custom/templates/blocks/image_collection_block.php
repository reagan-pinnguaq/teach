<?php if ( get_row_layout() == 'image_collection_block' ) : ?>
	<?php $image_collection_images = get_sub_field( 'image_collection' ); ?>
	<?php if ( $image_collection_images ) :  ?>
	<section class="image-collection-block grid">
		<?php foreach ( $image_collection_images as $image_collection_image ): ?>
			<a href="<?php echo $image_collection_image['url']; ?>">
				<img src="<?php echo $image_collection_image['sizes']['thumbnail']; ?>" alt="<?php echo $image_collection_image['alt']; ?>" />
			</a>
		<?php endforeach; ?>
	</section>	
	<?php endif; ?>
<?php endif;?>