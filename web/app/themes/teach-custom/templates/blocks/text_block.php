
<?php if(get_sub_field('columns') != 2):?>
<section class="text-block <?php if( isset($content) && $content != "introduction"):?>collapsed<?php endif;?>">
	<?php the_sub_field( 'text_1' ); ?>
</section>
<?php else:?>

<section class="text-block grid columns-2">
	<div><?php the_sub_field( 'text_1' ); ?></div>
	<div><?php the_sub_field( 'text_2' ); ?></div>
</section>
	
<?php endif;?>