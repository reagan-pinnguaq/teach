<?php if ( have_rows( 'card' ) ) : ?>	
		<?php $i=0; while ( have_rows( 'card' ) ) : the_row(); ?><?php	$i++; endwhile;?>
		<section class="simple-card-block grid card-count-<?=$i;?>">
			<h2><?php the_sub_field( 'card_group_title' ); ?></h2>

		<?php while ( have_rows( 'card' ) ) : the_row(); ?>
			<div class="card">
				<h3><?php the_sub_field( 'card_title' ); ?></h3>
				<p class="quotation-mark">“</p>
				<p><?php the_sub_field( 'card_body' ); ?></p>
				<a href="<?php the_sub_field( 'card_link' ); ?>">Read More</a>
			</div>
		<?php endwhile;?>
		</section>

<?php endif;?>