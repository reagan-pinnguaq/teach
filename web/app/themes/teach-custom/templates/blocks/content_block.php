<?php if ( have_rows(  $content_field ) ): ?>
	<?php while ( have_rows(  $content_field ) ) : the_row(); 
		 
		switch(get_row_layout()):
			case 'text_block': include(locate_template('templates/blocks/text_block.php')); break;
			case 'text_title_block': include(locate_template('templates/blocks/text_title_block.php')); break;
			case 'text_hero_block' : include(locate_template('templates/blocks/text_hero_block.php')); break;
			case 'text_image_block': include(locate_template('templates/blocks/text_image_block.php')); break;
			case 'link_block' : include(locate_template('templates/blocks/link_block.php')); break;
			case 'banner_block': include(locate_template('templates/blocks/banner_block.php')); break;
			case 'code_block': include(locate_template('templates/blocks/code_block.php')); break;
			case 'text_code_block': include(locate_template('templates/blocks/text_code_block.php')); break;
			case 'code_image_block': include(locate_template('templates/blocks/code_image_block.php')); break;
			case 'cards-block' : include(locate_template('templates/blocks/simple_card_block.php')); break;
			case 'image-block' : include(locate_template('templates/blocks/image_block.php')); break;
			case 'image_collection_block' : include(locate_template('templates/blocks/image_collection_block.php')); break;
			case 'article_block' : include(locate_template('templates/blocks/article_block.php')); break;
			case 'download_block' : include(locate_template('templates/blocks/download_block.php')); break;
			default: break;
		endswitch;

 endwhile; 
 ?>
<?php endif; ?>