<section class="text-block <?php if( isset($content) && $content != "introduction"):?>collapsed<?php endif;?>">
	<h2><?php the_sub_field( 'title' );?></h2>
	<?php the_sub_field( 'text_1' ); ?>
</section>