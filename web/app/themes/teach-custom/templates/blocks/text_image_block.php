<section class="text_image_block collapsed">
	<?php $left_image = get_sub_field( 'left_image' ); ?>
	<?php if ( $left_image ) { ?>
		<img src="<?php echo $left_image['url']; ?>" alt="<?php echo $left_image['alt']; ?>" />
	<?php } ?>
	<div class="image-text"><?php the_sub_field( 'text' ); ?></div>
	<?php $right_image = get_sub_field( 'right_image' ); ?>
	<?php if ( $right_image ) { ?>
		<img src="<?php echo $right_image['url']; ?>" alt="<?php echo $right_image['alt']; ?>" />
	<?php } ?>
</section>