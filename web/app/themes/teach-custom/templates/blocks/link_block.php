<?php $address = get_sub_field( 'address' ); ?>
<?php if ( $address ) : ?>
<section class="link-block grid <?php if( isset($content) && $content != "introduction"):?>collapsed<?php endif;?>">
	<a href="<?=$address['url']; ?>" target="<?=$address['target']; ?>"><?=$address['title']; ?></a>
</section>
<?php endif; ?>

