<?php $title = get_sub_field('title'); $text = get_sub_field('text');?>
<?php if($title && $text):?>
<section class="hero-text-block grid">
	<h2 class="text-center"><?=$title;?></h2>
	<p class="text-center"><?=$text;?></p>
	<div class="false-line"></div>
</section>
<?php endif;?>