<?php if ( get_row_layout() == 'download_block' ) : ?>
	<?php $download_file = get_sub_field( 'download_file' ); ?>
	<?php if ( $download_file ) { ?>
		<section class="download-block grid">
			<a class="" href="<?php echo $download_file['url']; ?>"><?php the_sub_field( 'text' ); ?></a>
		</section>
	<?php } ?>
<?php endif; ?>