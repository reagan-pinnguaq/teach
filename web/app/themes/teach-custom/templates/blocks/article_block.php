<?php if ( get_row_layout() == 'article_block' ) : ?>
<section class="article-preview-block">
	<?php $dt=  new \DateTime(str_replace('/', '-', get_sub_field( 'source_date' ))); $dt= $dt->format('F d, Y');?>
	<h3><?=$dt;?> <span class="dot"></span> <?php the_sub_field( 'source_name' ); ?></a></h3>
	<h2><a href="<?php the_sub_field('article_source_link');?>"><?php the_sub_field( 'article_title' ); ?></a></h2>
	<p><?php the_sub_field( 'text' ); ?>... <a href="<?php the_sub_field('article_source_link');?>">Read More</a></p>
</section>
<?php endif;?>