<?php global $lang;?>

<footer class="content-info grid">
  <nav class="grid">
    <a href="/" class="link-wrapper" role="Link back to the home page"><img src="<?=get_site_icon_url(); ?>" alt="<?=bloginfo('name');?> logo"><span class="sr-only"><?=bloginfo('name');?></span></a>
    <?php $lang->menu('footer', 'footer-nav', false, '', new Simplified_Walker_Nav_Menu());?>
    
    <?php wp_nav_menu(['theme_location'=>'social_navigation', 'menu_id'=>'social-icons', 'menu_class'=> 'grid', 'container' => false, 'walker' => new Custom_Walker_Nav_Menu()]);?>
    <?php get_template_part('partials/current-full-lang-selector');?>
  </nav>
  <section id="information" class="grid">
  <?php $lang->menu('information', 'information-items', false, '', new Simplified_Walker_Nav_Menu());?>
  <sub class="text-center">Copyright <?=date('Y');?> <?=bloginfo('name');?>. All rights reserved.</sub>
  </section>
</footer>
