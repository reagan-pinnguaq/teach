
<?php global $lang;?>

<?php if(is_archive()){$classes = 'achive-item grid';}?>
<article <?php post_class($classes); ?>>
  <?php if(get_post_type() == 'curriculum'):?>
  <header class="article-header grid">
    <h5 class="text-center"><?php the_category();?></h5>
    <a href="<?php the_permalink();?>" class="background-link" style="background-image: url('<?=get_field('featured_image');?>');"></a>
  </header>
  <section class="body">
    <a href="<?php the_permalink();?>"><h2 class="text-center"><?php $title = $lang->field('title', true); if($title){echo $title;} else {the_title();} ?></h2></a>
    <?php $content = $lang->prefix('introduction_content');?>
    <?php if ( have_rows( $content ) ): ?>
      <?php while ( have_rows( $content ) ) : the_row(); ?>
        <?php if ( get_row_layout() == 'text_block' ) : ?>
        <p><?=acf_excerpt(get_sub_field( 'text_1' ),24); ?></p>
        <?php break;?>
        <?php endif;?>
      <?php endwhile;?>
    <?php endif;?>
  </section>
  <footer class="grid">
    <a class="text-center text-upper" href="<?php the_permalink(); ?>">View Lesson</a>
  </footer>
  <?php else:?>
  <header class="article-header">
  <h2 class="entry-title text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
  <?php endif;?>

</article>