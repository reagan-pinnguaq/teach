<?php while (have_posts()) : the_post(); ?>
    <article class="grid page-content">
    <?php get_template_part('templates/page', 'header'); ?>
    <?php get_template_part('templates/content', 'page'); ?>
    <?php if(get_the_title() == 'Contact'):?>
        <?php get_template_part('partials/contact-form'); ?>
    <?php endif;?>
    <?php get_template_part('partials/page-footer-image');?>
    </article>
<?php endwhile; ?>

