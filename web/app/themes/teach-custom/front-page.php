<?php global $contentlang;?>
<?php $content = $lang->prefix('content');?>

<?php if ( have_rows( $content ) ): ?>
	<?php while ( have_rows( $content ) ) : the_row(); ?>
		<?php if ( get_row_layout() == 'cards-block' ) : ?>

				<?php get_template_part('templates/blocks/simple_card_block');?>
			
			<?php elseif ( get_row_layout() == 'image_block' ) : ?>
			<?php if ( get_sub_field( 'image' ) ) { ?>
				<img class="image-block" src="<?php the_sub_field( 'image' ); ?>" />
			<?php } ?>
		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<h2>Oops</h2>
	<p>There appears to be something wrong with this page. It does not appear to have any content</p>
	
<?php endif; ?>

<div class="angled-background"></div>
<!-- <img src="" alt=""> -->
