<?php
/**
 * Template Name: Flex Custom
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php // check if the flexible content field has rows of data
    if( have_rows('flex-content') ):

     // loop through the rows of data
    while ( have_rows('flex-content') ) : the_row();

        if( get_row_layout() == 'paragraph' ):

        	the_sub_field('text');

        elseif( get_row_layout() == 'download' ): 

        	$file = get_sub_field('file');

        endif;

    endwhile;

else :

    // no layouts found

endif;

?>
<?php endwhile; ?>
